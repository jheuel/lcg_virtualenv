# `virtualenv` using LCG views

[`virtualenv`](https://virtualenv.pypa.io/en/stable/) provides a convenient way
of managing Python environments however it cannot be easily used with LCG views.
This repository provides scripts to creating virtual environments with a custom
activation script to set up the underlying LCG view automatically. Additionally,
patches are used to avoid unexpected behaviour, such as when installing upgraded
packages on top of LCG.

## Setup

First clone this repository:

```bash
git clone https://gitlab.cern.ch/cburr/lcg_virtualenv.git
```

## Creating an environment

To create a virtual environment based on `LCG_94python3` run:

```bash
./lcg_virtualenv/create_lcg_virtualenv path/to/new/virtualenv
```

The version of LCG can also be explicitly specified, such as the Python 2
version of `LCG_94`:

```bash
./lcg_virtualenv/create_lcg_virtualenv path/to/new/virtualenv LCG_94
```

## Using an environment

Once created environments can be activated using:

```bash
source path/to/new/virtualenv/bin/activate
```

The original environment can be restored by running `deactivate`.

## Installing/upgrading python packages

Any packages installed using pip will override the LCG provided packages, for
example to:

 - get `pandas` from the underlying LCG view
 - upgrade `matplotlib` to version 3
 - install `root_pandas` which is not available from LCG

You can use:

```bash
pip install --upgrade root_pandas matplotlib
python -c 'import pandas; print(f"Got pandas from {pandas.__file__}")'
python -c 'import root_pandas; print(f"Got root_pandas from {root_pandas.__file__}")'
python -c 'import matplotlib; print(f"Got matplotlib from {matplotlib.__file__}")'
```

```
Collecting root_pandas
Collecting matplotlib
  Using cached https://files.pythonhosted.org/packages/ed/89/dd823436a5f8d5ca9304b51b554863bfd366ca84708d5812f5ee87c923bc/matplotlib-3.0.0-cp36-cp36m-manylinux1_x86_64.whl
Requirement already satisfied, skipping upgrade: pandas>=0.18.0 in /cvmfs/sft.cern.ch/lcg/views/LCG_94python3/x86_64-slc6-gcc62-opt/lib/python3.6/site-packages (from root_pandas) (0.23.3)
Requirement already satisfied, skipping upgrade: numpy in /cvmfs/sft.cern.ch/lcg/views/LCG_94python3/x86_64-slc6-gcc62-opt/lib/python3.6/site-packages (from root_pandas) (1.14.2)
Requirement already satisfied, skipping upgrade: root-numpy in /cvmfs/sft.cern.ch/lcg/views/LCG_94python3/x86_64-slc6-gcc62-opt/lib/python3.6/site-packages (from root_pandas) (4.7.3)
Requirement already satisfied, skipping upgrade: kiwisolver>=1.0.1 in /cvmfs/sft.cern.ch/lcg/views/LCG_94python3/x86_64-slc6-gcc62-opt/lib/python3.6/site-packages (from matplotlib) (1.0.1)
Requirement already satisfied, skipping upgrade: cycler>=0.10 in /cvmfs/sft.cern.ch/lcg/views/LCG_94python3/x86_64-slc6-gcc62-opt/lib/python3.6/site-packages (from matplotlib) (0.10.0)
Requirement already satisfied, skipping upgrade: python-dateutil>=2.1 in /cvmfs/sft.cern.ch/lcg/views/LCG_94python3/x86_64-slc6-gcc62-opt/lib/python3.6/site-packages (from matplotlib) (2.7.3)
Requirement already satisfied, skipping upgrade: pyparsing!=2.0.4,!=2.1.2,!=2.1.6,>=2.0.1 in /cvmfs/sft.cern.ch/lcg/views/LCG_94python3/x86_64-slc6-gcc62-opt/lib/python3.6/site-packages (from matplotlib) (2.2.0)
Requirement already satisfied, skipping upgrade: pytz>=2011k in /cvmfs/sft.cern.ch/lcg/views/LCG_94python3/x86_64-slc6-gcc62-opt/lib/python3.6/site-packages (from pandas>=0.18.0->root_pandas) (2018.5)
Requirement already satisfied, skipping upgrade: setuptools in /tmp/cburr/test-env/lib/python3.6/site-packages (from kiwisolver>=1.0.1->matplotlib) (40.4.3)
Requirement already satisfied, skipping upgrade: six in /cvmfs/sft.cern.ch/lcg/views/LCG_94python3/x86_64-slc6-gcc62-opt/lib/python3.6/site-packages (from cycler>=0.10->matplotlib) (1.11.0)
Installing collected packages: root-pandas, matplotlib
  Found existing installation: matplotlib 2.2.2
    Not uninstalling matplotlib at /cvmfs/sft.cern.ch/lcg/views/LCG_94python3/x86_64-slc6-gcc62-opt/lib/python3.6/site-packages, outside environment /tmp/cburr/test-env
    Can't uninstall 'matplotlib'. No files were found to uninstall.
Successfully installed matplotlib-3.0.0 root-pandas-0.6.0

Got pandas from /cvmfs/sft.cern.ch/lcg/views/LCG_94python3/x86_64-slc6-gcc62-opt/lib/python3.6/site-packages/pandas/__init__.py

Got root_pandas from /tmp/cburr/test-env/lib/python3.6/site-packages/root_pandas/__init__.py

Got matplotlib from /tmp/cburr/test-env/lib/python3.6/site-packages/matplotlib/__init__.py
```

## Removing an environment

Environments are entirely contained with in the folder specified during creation
and can therefore be removed by deleting the folder:

```bash
rm -rf path/to/new/virtualenv
```

## Tab completion (zsh only)

Add the `/path/to/lcg_virtualenv/completion` to the `fpath` array in your
`.zshrc` before `compinit` gets called. Or copy it to a directory that's
already in your `fpath`.
